<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}
</style>
</head>
<body>

<h2>USUÁRIOS</h2>

<table>
  <tr>
    <th>Nome</th>
  </tr>
  <tr>
    <td>Thiago</td>
  </tr>
  <tr>
    <td>Henrique</td>
  </tr>
  <tr>
    <td>Pedro</td>
  </tr>
  <tr>
    <td>João</td>
</tr>
</table>

</body>
</html>