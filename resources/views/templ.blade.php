
<html>
<title>Uma aba qualquer - @yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>

<div class="w3-container">
@section('sidebar')

  <div class="w3-card-4" style="width:50%;">
    <header class="w3-container w3-blue">
      <h1>Header</h1>
    </header>
@show
    <div class="w3-container">
    @yield('content')
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>

    <footer class="w3-container w3-blue">
      <h5>Footer</h5>
    </footer>
  </div>
</div>
</body>
</html>