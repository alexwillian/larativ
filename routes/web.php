<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Crie uma rota para a página de Blog*/
Route::get('/', function () {
    return view('blog');
});
/*Crie uma rota para acessar a página Administrativa*/

Route::get('admin/{name?}', function ($name = null) {
    return $name;
});

Route::get('admin/{name?}', function ($name = 'Will') {
    return $name;
});
/*Crie uma rota para listar os usuários: José, Maria, Pedro e João em uma tabela html*/
Route::get('/table', function () {
    echo ('<h1>Usuários:</h1>
    <table border="1px">
        <tr>
            <th><strong> Name: </strong></th>
            <td> José </td>
        </tr>
        <tr>
        <th><strong> Name: </strong></th>
        <td> Maria </td>
        
        </tr>
        <tr>
        <th><strong> Name: </strong></th>
        <td> Pedro </td>
        </tr>
        <tr>
        <th><strong> Name: </strong></th>
        <td> João </td>
        </tr>
    </table>'); 
});

/*Crie uma view que define um template...*/
Route::get('/templ', function () {
    return view('templ');
});
/*View que extende o template!*/
Route::get('/home', function () {
    return view('home');
});
/*Crie uma rota que leia dois nomes e mostre se os nomes são iguais*/
Route::get('/nomesiguais', function () {

    $nome1 = "Alex";
    $nome2 = "Nayla";

        if($nome1 == $nome2){
            echo "Os nomes são iguais";
        }else{
            echo "Os nomes são bem diferentes!";
        }

});
/*Crie uma rota que leia cinco parâmetros e tire a média dos valores*/
Route::get('/media', function () {
    function media(&$media) {
        $media = $media / 2;
        }
        $par1 = 10;
        $par2 = 9.0;
        $par3 = 7.5;
        $par4 = 9.0;
        $par5 = 7.5;
        $soma = $par1 + $par2 + $par3 + $par4 + $par5;
        media($soma);
        echo "Média dos cinco valores: $soma.";

});

/* Crie uma view com um Documento ou página básica em html*/
Route::get('/dochtml', function () {
    return view('dochtml');
});
/*Crie uma rota que receba três nomes e exibe em uma view*/
$this->view('/tresnomes', 'tresnomes', ['usuario1' => 'Willian','usuario2' => 'Géssica','usuario3' => 'Nayla']);

/*Crie uma rota que some dois números e exiba em uma view*/
$this->view('/soma', 'soma', ['valor1' => '4','valor2' => '3','soma'=>'4' + '3']);

/*Sugestão de rota: criar uma rota que some três números e subtraia pelo terceiro número respectivo, retornando em uma view*/
$this->view('/div', 'div', ['valor1' => '10','valor2' => '2', 'valor3'=> '5','soma'=>'10' + '2' + '5'-'2']);

/*Crie uma view que receba uma lista de usuários(Thiago, Henrique, Pedro e João) em uma tabela html*/
Route::get('/tableview', function () {
    return view ('tableview');
});